import numpy as np

# Примерные данные
data = {
    '2023-01-01': 5,
    '2023-01-02': 7,
    '2023-01-03': 8,
    '2023-01-04': 4,
    '2023-01-05': 6,
    '2023-01-06': 3,
    '2023-01-07': 7,
    '2023-01-08': 8,
    '2023-01-09': 9,
    '2023-01-10': 6,
    '2023-01-11': 4,
    '2023-01-12': 5,
    '2023-01-13': 2,
    '2023-01-14': 5,
    '2023-01-15': 7,
    '2023-01-16': 8,
    '2023-01-17': 9,
    '2023-01-18': 6,
    '2023-01-19': 4,
    '2023-01-20': 5,
    '2023-01-21': 3,
    '2023-01-22': 6,
    '2023-01-23': 7,
    '2023-01-24': 8,
    '2023-01-25': 9,
    '2023-01-26': 6,
    '2023-01-27': 4,
    '2023-01-28': 5,
    '2023-01-29': 3,
    '2023-01-30': 6,
    '2023-01-31': 7,
    '2023-02-01': 8,
    '2023-02-02': 9,
    '2023-02-03': 7,
    '2023-02-04': 6,
    '2023-02-05': 5,
    '2023-02-06': 4,
    '2023-02-07': 3,
    '2023-02-08': 6,
    '2023-02-09': 7,
    '2023-02-10': 8,
    '2023-02-11': 9,
    '2023-02-12': 6,
    '2023-02-13': 5,
    '2023-02-14': 4,
    '2023-02-15': 3,
    '2023-02-16': 6,
    '2023-02-17': 7,
    '2023-02-18': 8,
    '2023-02-19': 9,
    '2023-02-20': 7,
    '2023-02-21': 6,
    '2023-02-22': 5,
    '2023-02-23': 4,
    '2023-02-24': 3,
    '2023-02-25': 6,
    '2023-02-26': 7,
    '2023-02-27': 8,
    '2023-02-28': 9,
    '2023-03-01': 7,
    '2023-03-02': 6,
    '2023-03-03': 5,
    '2023-03-04': 4,
    '2023-03-05': 3,
    '2023-03-06': 6,
    '2023-03-07': 7,
    '2023-03-08': 8,
    '2023-03-09': 9,
    '2023-03-10': 7,
    '2023-03-11': 6,
    '2023-03-12': 5,
    '2023-03-13': 4,
    '2023-03-14': 3,
    '2023-03-15': 6,
    '2023-03-16': 7,
    '2023-03-17': 8,
    '2023-03-18': 9,
    '2023-03-19': 7,
    '2023-03-20': 6,
    '2023-03-21': 5,
    '2023-03-22': 4,
    '2023-03-23': 3,
    '2023-03-24': 6,
    '2023-03-25': 7,
    '2023-03-26': 8,
    '2023-03-27': 9,
    '2023-03-28': 7,
    '2023-03-29': 6,
    '2023-03-30': 5,
    '2023-03-31': 4
}

def filter_invalid_data(data):
    return {date: temp for date, temp in data.items() if temp is not None}
def calculate_deviations(data):
    mean_temp = np.mean(list(data.values()))
    return {date: temp - mean_temp for date, temp in data.items()}


def find_anomaly_days(data, threshold=2):
    deviations = calculate_deviations(data)
    std_deviation = np.std(list(deviations.values()))
    anomaly_count = sum(1 for deviation in deviations.values() if deviation > threshold * std_deviation)
    total_days = len(data)
    anomaly_fraction = anomaly_count / total_days
    return anomaly_fraction
def analyze_climate_data(data):
    filtered_data = filter_invalid_data(data)
    transformed_data = calculate_deviations(filtered_data)
    anomaly_fraction = find_anomaly_days(transformed_data)
    return anomaly_fraction

anomaly_fraction = analyze_climate_data(data)
print(f"Доля дней с аномально высокой температурой: {anomaly_fraction:.2%}")
